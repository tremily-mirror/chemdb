#!/usr/bin/env python
#
# Copyright

"""Generate a complete inventory for every storage location.

You may need to export ``DJANGO_SETTINGS_MODULE`` so that the script
can find the appropriate database.
"""

import chemdb.models as _models
import chemdb.doc as _doc


def filename(location, extension='.pdf'):
    loc = location.abbrev.replace(' ', '-')
    return '{}{}'.format(loc, extension)


def generate_inventories():
    "Per-location inventories for each location."
    for location in _models.Location.objects.all():
        chemical_instances = location.chemical_instances.all()
        dg = _doc.DocGen(chemical_instances=chemical_instances)        
        pdf = dg.inventory(title='Inventory ({})'.format(location.abbrev))
        with open(filename(location), 'wb') as f:
            f.write(pdf)

def generate_inventory():
    "Single inventory for the whole lab"
    chemical_instances = _models.ChemicalInstance.objects.all()
    dg = _doc.DocGen(chemical_instances=chemical_instances)        
    pdf = dg.inventory()
    with open('inventory.pdf', 'wb') as f:
        f.write(pdf)

def generate_door_warning():
    "Door warning for the whole lab"
    chemical_instances = _models.ChemicalInstance.objects.all()
    dg = _doc.DocGen(chemical_instances=chemical_instances)        
    pdf = dg.door_warning()
    with open('door-warning.pdf', 'wb') as f:
        f.write(pdf)    

if __name__ == '__main__':
    generate_inventories()
    generate_inventory()
    generate_door_warning()
