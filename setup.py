# Copyright (C) 2012 W. Trevor King
#
# This file is part of update-copyright.
#
# update-copyright is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# update-copyright is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with update-copyright.  If not, see
# <http://www.gnu.org/licenses/>.

"Track chemical inventories and produce inventories and door warnings."

import codecs as _codecs
from distutils.core import setup as _setup
import os.path as _os_path

from chemdb import __version__


_this_dir = _os_path.dirname(__file__)

_setup(
    name='chemdb',
    version=__version__,
    maintainer='W. Trevor King',
    maintainer_email='wking@drexel.edu',
    url='http://blog.tremily.us/posts/ChemDB/',
    download_url='http://git.tremily.us/?p=chemdb.git;a=snapshot;h={};sf=tgz'.format(__version__),
    license='GNU General Public License (GPL)',
    platforms=['all'],
    description=__doc__,
    long_description=_codecs.open(
        _os_path.join(_this_dir, 'README'), 'r', encoding='utf-8').read(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Intended Audience :: Science/Research',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Programming Language :: Python',
        'Topic :: Database',
        'Topic :: Scientific/Engineering :: Chemistry',
        ],
    packages=['chemdb', 'chemdb.templatetags'],
    provides=['chemdb', 'chemdb.templatetags'],
    package_data = {
        'chemdb': [
            'fixtures/*',
            'static/chemdb/*',
            'templates/chemdb/*.html',
            'templates/chemdb/doc/*',
            ]},
    )
