% nfpa_704.sty, a package for generating NFPA fire diamonds.
% Version 0.1
%
% Copyright (C) 2010 W. Trevor King <wking@drexel.edu>
%
% This file is part of ChemDB.
%
% ChemDB is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
%
% ChemDB is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with ChemDB.  If not, see <http://www.gnu.org/licenses/>.

% See
%   http://en.wikipedia.org/wiki/NFPA_704
% for more information on the NFPA 704 "fire diamond".

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesPackage{nfpa_704}
          [2010/08/24 v0.1  NFPA flammability diamond]

\RequirePackage{forloop} % program flow control
\RequirePackage{tikz} % graphics
\RequirePackage{ulem} % strikethrough (or strikeout)
\RequirePackage{xkeyval} % handling x=y-type options
\normalem % use the standard \em, not ulem's version

% Raise \ulem's \sout height a bit to look nicer striking out capital
% letters like "W".
\def\nfpa@caps@sout{\bgroup \ULdepth=-1ex \ULset}

\newcommand{\nowater}{\nfpa@caps@sout{W}}
\newcommand{\oxidizer}{OXY}
\newcommand{\asphixiant}{SA}

\newcommand{\nfpa@firediamond@radius}{}
\newcommand{\nfpa@firediamond@health}{}
\newcommand{\nfpa@firediamond@flammability}{}
\newcommand{\nfpa@firediamond@reactivity}{}
\newcommand{\nfpa@firediamond@extraspecial}{}

\define@key{firediamond}{radius}{\def\nfpa@firediamond@radius{#1}}
\define@key{firediamond}{health}{\def\nfpa@firediamond@health{#1}}
\define@key{firediamond}{flammability}{\def\nfpa@firediamond@flammability{#1}}
\define@key{firediamond}{reactivity}{\def\nfpa@firediamond@reactivity{#1}}
\define@boolkey{firediamond}{nowater}[true]{}
\define@boolkey{firediamond}{oxidizer}[true]{}
\define@boolkey{firediamond}{asphixiant}[true]{}
\define@key{firediamond}{special}{\def\nfpa@firediamond@extraspecial{#1}}
\presetkeys{firediamond}{% set default values
  radius=3.5em,
  health=0,flammability=0,reactivity=0,
  nowater=false,oxidizer=false,asphixiant=false,
  special={}}%
  {}% post-process input parameters

\newcounter{nfpa@firediamond@ispecial}
\newcounter{nfpa@firediamond@jspecial}
\newcommand{\nfpa@firediamond@clearspecial}{%
  \setcounter{nfpa@firediamond@ispecial}{0}%
}

% Add a special string (e.g. \nowater, \oxidizer, ...).
\newcommand{\nfpa@firediamond@addspecial}[1]{%
  \addtocounter{nfpa@firediamond@ispecial}{1}%
  \expandafter\gdef\csname nfpa@firediamond@special@\alph{nfpa@firediamond@ispecial}\endcsname{#1}%
}

% Add a special string, evaluating the value of #1 immediately.  This
% is important in the case of \nfpa@firediamond@makespecial's \@ii, as
% it will be set to \@nil after looping.  However, it will fail for some
% macros.  For example, \nfpa@firediamond@xaddspecial{\nowater} raises
%   ! Undefined control sequence.
%   \ULset ...ce \ULdepth .4\p@ \fi \def \UL@leadtype 
%                                                    {\leaders \hrule \@height ...
\newcommand{\nfpa@firediamond@xaddspecial}[1]{%
  \addtocounter{nfpa@firediamond@ispecial}{1}%
  \expandafter\xdef\csname nfpa@firediamond@special@\alph{nfpa@firediamond@ispecial}\endcsname{#1}%
}

\newcommand{\nfpa@firediamond@getspecial}[1]{%
  \csname nfpa@firediamond@special@\alph{#1}\endcsname%
}

% Print the contents of the special quadrant and beyond.
%
% From http://www.nfpa.org/assets/files/PDF/ROP/704-A2006-ROP.pdf
%
%  704-4 Log #CP4                     Final Action: Accept
%  (8.2.3 and Figures 9.1(a) and (c))
%  ...
%  SUBSTANTIATION: The committee expressed concern that for the
%  relatively few chemicals requiring both an "OX" and "W" symbols in
%  the special hazards quadrant, there wouldn't be enough room for
%  both to appear in the prescribed sizes for clear visibility. The
%  committee believed the "W" is the primary hazard and should be
%  displayed inside the quadrant, with the "OX" outside the
%  quadrant. It was reported that many large companies have adopted
%  this practice successfully already. The figures were enhanced for
%  user-friendliness.
\newcommand{\nfpa@firediamond@makespecial}{%
  \nfpa@firediamond@clearspecial{}%
  \ifKV@firediamond@nowater\nfpa@firediamond@addspecial{\nowater}\fi%
  \ifKV@firediamond@oxidizer\nfpa@firediamond@addspecial{\oxidizer}\fi%
  \ifKV@firediamond@asphixiant\nfpa@firediamond@addspecial{\asphixiant}\fi%
  % Split commas.  See
  %   http://www.tex.ac.uk/ctan/macros/latex/base/ltcntrl.dtx
  % for details on LaTeX program control macros. 
  \@for\@ii:=\nfpa@firediamond@extraspecial\do{%
    \nfpa@firediamond@xaddspecial{\@ii}}%
}

% Print the contents of the special quadrant.
\newcommand{\nfpa@firediamond@special}{%
  \ifnum\value{nfpa@firediamond@ispecial}>0%
    \setcounter{nfpa@firediamond@jspecial}{1}%
    \nfpa@firediamond@getspecial{nfpa@firediamond@jspecial}%
  \fi%
}

% Print special contents to be printed below the special quadrant.
\newcommand{\nfpa@firediamond@underspecial}{%
  \begingroup%
    \setlength{\parskip}{0pt}%
    \setlength{\parindent}{0pt}%
    \addtocounter{nfpa@firediamond@ispecial}{1}%
    \forloop{nfpa@firediamond@jspecial}% counter
      {2}% initial value
      {\value{nfpa@firediamond@jspecial} < \value{nfpa@firediamond@ispecial}}% condition
      {\nfpa@firediamond@getspecial{nfpa@firediamond@jspecial}\\}% code
    \addtocounter{nfpa@firediamond@ispecial}{-1}%
  \endgroup%
}

\newcommand{\firediamond}[1]{%
  \setkeys{firediamond}{#1}%
  \nfpa@firediamond@makespecial{}%
  % Simple text output for debugging xkeyval parameter setup.
  %Health: \nfpa@firediamond@health \\
  %Flammability: \nfpa@firediamond@flammability \\
  %Reactivity: \nfpa@firediamond@reactivity \\
  %Special: \par\nfpa@firediamond@special\par
  %
  % Draw the NFPA diamond
  \begin{tikzpicture}[x=\nfpa@firediamond@radius, y=\nfpa@firediamond@radius]
    % draw the background colors
    \fill[blue]   (0,0) -- (-0.5,-0.5) -- (-1,0) -- (-0.5,0.5)  -- cycle;
    \fill[red]    (0,0) -- (-0.5,0.5)  -- (0,1)  -- (0.5,0.5)   -- cycle;
    \fill[yellow] (0,0) -- (0.5,0.5)   -- (1,0)  -- (0.5,-0.5)  -- cycle;
    \fill[white]  (0,0) -- (0.5,-0.5)  -- (0,-1) -- (-0.5,-0.5) -- cycle;
    %
    % draw the borders
    \draw (-0.5,-0.5) -- (0.5,0.5);
    \draw (-0.5,0.5) -- (0.5,-0.5);
    \draw (-1,0) -- (0,1) -- (1,0) -- (0,-1) -- cycle;
    %
    % add the text
    \draw (-0.5,0)  node { \nfpa@firediamond@health};
    \draw (0,0.5)  node { \nfpa@firediamond@flammability};
    \draw (0.5,0)  node { \nfpa@firediamond@reactivity};
    \draw (0,-0.5) node[text width=\nfpa@firediamond@radius, text centered]
          { \nfpa@firediamond@special};
    \draw (0,-1) node[text width=\nfpa@firediamond@radius, text centered,
                      anchor=north]
          { \nfpa@firediamond@underspecial};
  \end{tikzpicture}
}
