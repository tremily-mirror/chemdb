# Copyright

"""Run tests with ``manage.py test``.
"""

from django.forms import ValidationError as _ValidationError
from django.test import _TestCase as _TestCase
from . import models as _models


#class UnitTest(TestCase):
#    def setUp(self):
#        self.F = models.Unit(
#            name=u'degree Farenheit', abbrev=u'\u00b0F', type=u'temperature',
#            system=models.US, scale=1/1.8, offset=32)
#        self.gal = models.Unit(
#            name=u'gallon', abbrev=u'gal', type=u'volume',
#            system=models.US, scale=3.78541, offset=0)
#
#    def test_conversion_from_si(self):
#        "Test from-SI conversion"
#        self.assertEqual(self.F.convert_from_si(0), 32)
#        self.assertEqual(self.F.convert_from_si(100), 212)
#        self.assertEqual(self.gal.convert_from_si(1), 0.26417217685798894)
#
#    def test_conversion_to_si(self):
#        "Test to-SI conversion"
#        self.assertEqual(self.F.convert_to_si(32), 0)
#        self.assertEqual(self.F.convert_to_si(212), 100)
#        self.assertEqual(self.gal.convert_to_si(1), 3.78541)
#
#    def test_formatting(self):
#        "Test amount formatting"
#        self.assertEqual(unicode(self.gal), u'gal')
#        self.assertEqual(unicode(self.F), u'\u00b0F')
#
#
#class AmountTest(TestCase):
#    def setUp(self):
#        self.unit = models.Unit(
#            name=u'gallon', abbrev=u'gal', type=u'volume',
#            system=models.US, scale=3.78541, offset=0)
#        self.amount = models.Amount()
#
#    def test_formatting(self):
#        "Test amount formatting"
#        for v,minv,maxv,unit,result in (
#            (None, None, None, None, u'-'),
#            (None, None, None, self.unit, u'- gal'),
#            (2,    None, None, self.unit, u'2 gal'),
#            (2,     1.5, None, self.unit, u'1.5-2 gal'),
#            (2,    None,  2.5, self.unit, u'2-2.5 gal'),
#            (2,     1.5,  2.5, self.unit, u'2 (1.5-2.5) gal'),
#            (None,  1.5,  2.5, self.unit, u'1.5-2.5 gal'),
#            ):
#            self.amount.unit = unit
#            self.amount.value = v
#            self.amount.min_value = minv
#            self.amount.max_value = maxv
#            self.assertEqual(self.amount.format_amount(), result)
#
#    def test_invalid_formatting(self):
#        "Test amount formatting which raises errors"
#        for v,minv,maxv,unit,result in (
#            (None,  1.5, None, self.unit, u'2 gal'),
#            (None, None,  2.5, self.unit, u'2 gal'),
#            ):
#            self.amount.unit = unit
#            self.amount.value = v
#            self.amount.min_value = minv
#            self.amount.max_value = maxv
#            self.assertRaises(ValidationError, self.amount.format_amount)
#
#    def test_validation(self):
#        "Test amount validation"
#        for valid,v,minv,maxv,unit in (
#            (True,  None, None, None, None),
#            (True,  None, None, None, self.unit),
#            (True,  2,    None, None, self.unit),
#            (True,  2,     1.5, None, self.unit),
#            (True,  2,    None,  2.5, self.unit),
#            (True,  2,     1.5,  2.5, self.unit),
#            (True,  None,  1.5,  2.5, self.unit),
#            (False, None,  1.5, None, self.unit),
#            (False, None, None,  2.5, self.unit),
#            ):
#            self.amount.unit = unit
#            self.amount.value = v
#            self.amount.min_value = minv
#            self.amount.max_value = maxv
#            if valid:
#                self.amount.clean()
#            else:
#                self.assertRaises(ValidationError, self.amount.clean)

