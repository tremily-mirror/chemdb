# Copyright

from django.db import models as _models
from django.forms import ValidationError as _ValidationError

from . import LOG as LOG
from . import util as _util


class NamedItem (_models.Model):
    name = _models.CharField(max_length=100)
    abbrev = _models.CharField('abbreviation', max_length=20)

    class Meta:
        abstract = True
        ordering = ['abbrev', 'name']

    def __unicode__(self):
        return u'{0.abbrev}'.format(self)


class NFPASpecial (NamedItem):
    """An NFPA Special rating (e.g. 'OX', '-W-', 'SA', ...).
    """
    symbol = _models.CharField(max_length=3, blank=True, null=True)

    def __unicode__(self):
        if self.symbol:
            return u'{0.symbol}'.format(self)
        return super(NFPASpecial, self).__unicode__()


class CASNumber (NamedItem):
    "Chemical Abstracts Service registery number"
    cas = _models.CharField(
        'CAS#', max_length=20, unique=True)

    def clean(self):
        if not _util.valid_CASno(self.cas):
            raise _ValidationError("invalid CAS number '{}'".format(self.cas))


class Chemical (NamedItem):
    """A chemical (in the abstract, not an instance of the chemical)

    Separating ``Chemical``\s from ``ChemicalInstance``\s avoids
    duplicate information (e.g. you can have two bottles of acetic
    acid).
    """
    cas = _models.ManyToManyField(
        CASNumber, blank=True, null=True, related_name='chemicals')
    msds = _models.FileField(
        'Material safety data sheet', upload_to=_util.chemical_upload_to,
        blank=True, null=True)
    health = _models.PositiveIntegerField(
        'NFPA health rating', blank=True, null=True)
    fire = _models.PositiveIntegerField(
        'NFPA fire rating', blank=True, null=True)
    reactivity = _models.PositiveIntegerField(
        'NFPA reactivity rating', blank=True, null=True)
    special = _models.ManyToManyField(
        NFPASpecial, blank=True, null=True, related_name='chemicals')
    mutagen = _models.NullBooleanField()
    carcinogen = _models.NullBooleanField()
    teratogen = _models.NullBooleanField()
    note = _models.TextField('notes', blank=True, null=True)

    def cas_numbers(self):
        if self.cas.count() == 0:
            return 'unknown'
        return ', '.join(cas.cas for cas in self.cas.all())

    def specials(self):
        return ' '.join(str(special) for special in self.special.all())


class Location (NamedItem):
    "A chemical storage location (e.g. 'acidic liquids')"
    pass


class Vendor (NamedItem):
    "A chemical supplier"
    url = _models.URLField('URL', blank=True, null=True)
    note = _models.TextField('notes', blank=True, null=True)


class ChemicalInstance (_models.Model):
    """An instance of a ``Chemical``

    For example, 1L of acetic acid from Vendor X.
    """
    chemical = _models.ForeignKey(Chemical, related_name='chemical_instances')
    location = _models.ForeignKey(Location, related_name='chemical_instances')
    amount = _models.CharField(max_length=100)
    vendor = _models.ForeignKey(Vendor, related_name='chemical_instances')
    catalog = _models.CharField('vendor catalog number', max_length=100)
    received = _models.DateField(auto_now_add=True, editable=True)
    disposed = _models.DateField(blank=True, null=True)

    class Meta:
        ordering = ['chemical', 'received', 'disposed', 'id']
