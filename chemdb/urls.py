# Copyright

from django.conf import settings as _settings
from django.conf.urls import defaults as _defaults
from django.views import generic as _generic
from django.contrib import admin as _admin

# If you're not serving the static content via some other server
from django.conf.urls.static import static as _static

from . import models as _models
from . import views as _views


_admin.autodiscover()


urlpatterns = _defaults.patterns(
    '',
    _defaults.url(r'^$', _views.static_context_list_view_factory(
            extra_context={'title': 'Chemical instances'},
            ).as_view(
            queryset=_models.ChemicalInstance.objects.all(),
            context_object_name='chemical_instances',
            template_name='chemdb/chemical_instances.html'),
        name='chemical_instances'),
    _defaults.url(r'^chemical/(?P<pk>\d+)/$', _generic.DetailView.as_view(
            model=_models.Chemical, template_name='chemdb/chemical.html'),
        name='chemical'),
    _defaults.url(r'^doc/(?P<target>\w*)$', _views.doc_page, name='doc'),

    # Uncomment the admin/doc line below to enable admin documentation:
    #_defaults.url(
    #    r'^admin/doc/', _defaults.include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    _defaults.url(
        r'^admin/', _defaults.include(_admin.site.urls), name='admin'),
    _defaults.url(
        r'^grappelli/', _defaults.include('grappelli.urls'), name='admin'),

    _defaults.url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to',
        kwargs={'url': _settings.STATIC_URL + 'chemdb.ico'}),
    ) + _static(_settings.MEDIA_URL, document_root=_settings.MEDIA_ROOT)

