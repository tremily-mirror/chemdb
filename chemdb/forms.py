# Copyright

from django import forms as _forms

from . import models as _models


class LocationsForm (_forms.Form):
    def __init__(self, *args, **kwargs):
        choices = [(x.id, x.name) for x in _models.Location.objects.all()]
        super(LocationsForm, self).__init__(*args, **kwargs)
        self.fields['location'] = _forms.MultipleChoiceField(
            widget=_forms.CheckboxSelectMultiple,
            choices=choices, label='Locations')
