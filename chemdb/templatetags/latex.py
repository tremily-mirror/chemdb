# Copyright

import sys as _sys

from django import template as _template


register = _template.Library()


LATEX_REPLACEMENTS = [
    ('%', r'\%'), ('>', '$>$'), ('<', '$<$'), (u'\u2026', r'\ldots'),
    (u'\u22c5', '$\cdot$')]
_SUPERSCRIPT_CODEPOINTS = {1: 0x00B9, 2: 0x00B2, 3: 0x00B3}
for i in range(9):
    subscript_codepoint = 0x2080 + i
    superscript_codepoint = _SUPERSCRIPT_CODEPOINTS.get(i, 0x2070 + i)
    if _sys.version_info >= (3,):
        subscript = chr(subscript_codepoint)
        superscript = chr(superscript_codepoint)
    else:
        subscript = unichr(subscript_codepoint)
        superscript = unichr(superscript_codepoint)
    LATEX_REPLACEMENTS.extend([
            (subscript, '$_{{{}}}$'.format(i)),
            (superscript, '$^{{{}}}$'.format(i)),
            ])
del i, subscript_codepoint, superscript_codepoint, subscript, superscript

LATEX_SPECIALS = {  # abbrev -> (latex, nfpa_704:firediamond argument)
    'OX': (r'\oxidizer', 'oxidizer'),
    '-W-': (r'\nowater', 'nowater'),
    'SA': (r'\asphixiant', 'asphixiant'),
    }  # others default to ('{abbrev}', '{abbrev}')


@register.filter
def latex_safe(string):
    for a,b in LATEX_REPLACEMENTS:
        string = string.replace(a, b)
    return string

@register.filter
def latex_specials(specials):
    """Format specials for general LaTeX usage
    """
    special_abbrevs = set(special.abbrev for special in specials)
    latex_specials = []
    for abbrev in ['OX', '-W-', 'SA']:
        if abbrev in special_abbrevs:
            latex_specials.append(LATEX_SPECIALS[abbrev][0])
            special_abbrevs.remove(abbrev)
    if special_abbrevs:  # leftovers:
        special_abbrevs = sorted(special_abbrevs)
        for abbrev in special_abbrevs:
            latex_specials.append(
                LATEX_SPECIALS.get(abbrev, ('{{{}}}'.format(abbrev), None))[0])
    return '~'.join(latex_specials)

@register.filter
def latex_special_args(specials):
    """Format specials for the NFPA firediamond.
    """
    special_abbrevs = set(special.abbrev for special in specials)
    latex_specials = []
    for abbrev in ['OX', '-W-', 'SA']:
        if abbrev in special_abbrevs:
            latex_specials.append(LATEX_SPECIALS[abbrev][1])
            special_abbrevs.remove(abbrev)
    if special_abbrevs:  # leftovers:
        special_abbrevs = sorted(special_abbrevs)
        latex_specials.append('special={{{}}}'.format(
                ','.join(special_abbrevs)))
    return ', '.join(latex_specials)
