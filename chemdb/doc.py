# Copyright (C) 2010 W. Trevor King <wking@drexel.edu>
#
# This file is part of ChemDB.
#
# ChemDB is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# ChemDB is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChemDB.  If not, see <http://www.gnu.org/licenses/>.

"""Generate inventories and other required documentation.
"""

import collections as _collections
import os as _os
import os.path as _os_path
import shutil as _shutil
import subprocess as _subprocess
import tempfile as _tempfile
import time as _time
import types as _types

from django.template import loader as _loader

from . import LOG as _LOG
from .templatetags import latex as _latex


class DocGen (object):
    "Generate the officially required documents"
    def __init__(self, chemical_instances):
        self.chemical_instances = chemical_instances

    def _make_pdf(self, files):
        assert 'main.tex' in files, files.keys()
        tmp_dir = None
        try:
            tmp_dir = _tempfile.mkdtemp()
            for filename,content in files.items():
                with open(_os_path.join(tmp_dir, filename), 'wb') as f:
                    f.write(content)
            status,stdout,stderr = self._invoke_pdflatex(cwd=tmp_dir)
            for i in range(3):
                if 'Rerun to get cross-references right' not in stdout:
                    break
                status,stdout,stderr = self._invoke_pdflatex(cwd=tmp_dir)
            pdf = open(_os_path.join(tmp_dir, 'main.pdf'), 'rb').read()
        finally:
            if tmp_dir:
                _shutil.rmtree(tmp_dir)
        return pdf

    def _invoke_pdflatex(self, **kwargs):
        p = _subprocess.Popen(
            ['pdflatex', '-interaction=nonstopmode', 'main.tex'],
            stdin=_subprocess.PIPE, stdout=_subprocess.PIPE,
            stderr=_subprocess.PIPE, shell=False, close_fds=True, **kwargs)
        stdout,stderr = p.communicate()
        status = p.wait()
        if status:
            raise RuntimeError((status, stdout, stderr))
        return (status, stdout, stderr)

    def inventory(self, title=None):
        """Create a pdf list of all chemicals.
        """
        if title == None:
            title == 'Inventory'
        tex = _loader.render_to_string(
            'chemdb/doc/inventory.tex', {
                'title': title,
                'chemical_instances': self.chemical_instances,
                })
        nfpa_704 = _loader.render_to_string('chemdb/doc/nfpa_704.sty')
        return self._make_pdf({'main.tex': tex, 'nfpa_704.sty': nfpa_704})

    def door_warning(self):
        """Create an NFPA diamond and list of the most dangerous chemicals.
        """
        nfpa_max = {'health':0, 'fire':0, 'reactivity':0, 'special':set()}
        nasties = _collections.defaultdict(list)
        nasties['special'] = set()
        for chemical_instance in self.chemical_instances:
            for attr in ['health', 'fire', 'reactivity', 'special',
                         'mutagen', 'carcinogen', 'teratogen']:
                value = getattr(chemical_instance.chemical, attr)
                if not value:
                    continue
                if attr in ['health', 'fire', 'reactivity']:
                    if value > nfpa_max[attr]:
                        nfpa_max[attr] = value
                        nasties[attr] = [chemical_instance]
                    elif value == nfpa_max[attr]:
                        nasties[attr].append(chemical_instance)
                elif attr == 'special':
                    if value.count():
                        nfpa_max[attr].update(value.all())
                        nasties[attr].add(chemical_instance)
                else:
                    nasties[attr].append(chemical_instance)

        instance_groups = [
            ('Health: {}'.format(nfpa_max['health']), nasties['health']),
            ('Fire: {}'.format(nfpa_max['fire']), nasties['fire']),
            ('Reactivity: {}'.format(nfpa_max['reactivity']),
             nasties['reactivity']),
            ('Special: {}'.format(_latex.latex_specials(nfpa_max['special'])),
             nasties['special']),
            ('Mutagen', nasties['mutagen']),
            ('Carcinogen', nasties['carcinogen']),
            ('Teratogen', nasties['teratogen']),
            ]
        tex = _loader.render_to_string(
            'chemdb/doc/door.tex', {
                'health': nfpa_max['health'],
                'fire': nfpa_max['fire'],
                'reactivity': nfpa_max['reactivity'],
                'special': nfpa_max['special'],
                'instance_groups': instance_groups,
                })

        nfpa_704 = _loader.render_to_string('chemdb/doc/nfpa_704.sty')
        return self._make_pdf({'main.tex': tex, 'nfpa_704.sty': nfpa_704})
