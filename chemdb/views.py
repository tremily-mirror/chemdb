# Copyright

from django import http as _http
from django import shortcuts as _shortcuts
from django import template as _template
from django.views import generic as _generic

from . import doc as _doc
from . import forms as _forms
from . import models as _models


class StaticContextListView (_generic.ListView):
    _name_counter = 0
    def get_context_data(self, **kwargs):
        context = super(StaticContextListView, self).get_context_data(**kwargs)
        context.update(self._extra_context)
        return context

def static_context_list_view_factory(extra_context):
    class_name = 'StaticContextListView_{:d}'.format(
        StaticContextListView._name_counter)
    StaticContextListView._name_counter += 1
    class_bases = (StaticContextListView,)
    class_dict = dict(StaticContextListView.__dict__)
    new_class = type(class_name, class_bases, class_dict)
    new_class._extra_context = extra_context
    return new_class

def doc_page(request, target=None):
    context = _template.RequestContext(request)
    if target in [None, '', 'index']:
        context['locations_form'] = _forms.LocationsForm()
        return _shortcuts.render_to_response('chemdb/doc.html', context)
    elif target in ['inventory', 'door']:
        pass
    else:
        raise _http.Http404()
    locations = [_shortcuts.get_object_or_404(_models.Location, pk=int(id))
                 for id in request.GET.getlist('location')]
    if locations:
        chemical_instances = []
        for location in locations:
            chemical_instances.extend(location.chemical_instances.all())
        chemical_instances.sort()
    else:
        chemical_instances = _models.ChemicalInstance.objects.all()
    dg = _doc.DocGen(chemical_instances=chemical_instances)
    if target == 'inventory':
        pdf = dg.inventory(title='Inventory')
    else:
        pdf = dg.door_warning()
    response = _http.HttpResponse(mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; filename={}.pdf'.format(
        target)
    response.write(pdf)
    return response
