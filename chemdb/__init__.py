# Copyright

import logging as _logging
import logging.handlers as _logging_handlers


__version__ = '0.5'


LOG = _logging.getLogger('chemdb')
LOG.setLevel(_logging.ERROR)
LOG.addHandler(_logging_handlers.SysLogHandler(address='/dev/log'))
