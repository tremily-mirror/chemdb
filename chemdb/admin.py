# Copyright

from django import forms as _forms
from django.contrib import admin as _admin
from django.db import models as _django_models

from . import models as _models


#class ChemicalInline (_admin.TabularInline):
#    model = _models.Chemical
#    extra = 0
#
#
#class IngredientBlockAdmin (_admin.ModelAdmin):
#    fieldsets = [
#        (None, {'fields': ['name']}),
#        ('Directions', {'fields': ['directions_markdown'],
#                        'classes': ['collapse']}),
#        ]
#    inlines = [IngredientInline]
#
#    list_display = ['name', 'recipe']
#    extra = 0
#
#
#class IngredientBlockInline (admin.TabularInline):
#    model = models.IngredientBlock
#    fieldsets = [
#        (None, {'fields': ['name', 'position']}),
#        ]
#    sortable_field_name = 'position'
#    inlines = [IngredientInline]
#    list_display = ['name']
#    extra = 0
#    show_edit_link = True  # https://code.djangoproject.com/ticket/13163
#    # work around 13163
#    #template = 'admin/edit_inline/tabular-13163.html'
#
#class RecipeAdmin (admin.ModelAdmin):
#    fieldsets = [
#        (None, {'fields': ['name']}),
#        ('Metadata', {'fields': ['author', 'source', 'url', 'tags'],
#                      'classes': ['collapse']}),
#        ('Yield', {'fields': ['unit', 'value', 'min_value', 'max_value'],
#                   'classes': ['collapse']}),
#        ('Directions', {'fields': ['directions_markdown']}),
#        ]
#    inlines = [IngredientBlockInline]
#
#    list_display = ['name']
#

#_admin.site.register(_models.Recipe, RecipeAdmin)
#_admin.site.register(_models.IngredientBlock, IngredientBlockAdmin)
_admin.site.register(_models.NFPASpecial)
_admin.site.register(_models.CASNumber)
_admin.site.register(_models.Chemical)
_admin.site.register(_models.Location)
_admin.site.register(_models.Vendor)
_admin.site.register(_models.ChemicalInstance)
