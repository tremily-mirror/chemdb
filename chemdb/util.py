# Copyright

import os.path as _os_path
import re as _re

from django.template.defaultfilters import slugify as _slugify

from . import LOG as _LOG


CAS_REGEXP = _re.compile('\A[0-9]{2,}[-][0-9]{2}[-][0-9]\Z')


def valid_CASno(cas_string):
    """Validate CAS numbers.

    Check `N..NN-NN-N` format, and the `checksum digit`_ for valid CAS
    number structure.  for

    .. math::
      N_n .. N_4 N_3 - N_2 N_1 - R

    The checksum digit is

    .. math::
      R = remainder([sum_{i=1}^n i N_i  ] / 10 )

    .. _checksum digit:
      http://www.cas.org/expertise/cascontent/registry/checkdig.html

    >>> valid_CASno('107-07-3')
    True
    >>> valid_CASno('107-08-3')
    False
    >>> valid_CASno('107-083')
    False

    Sometimes we don't have a CAS number, or a product will contain
    secret, non-hazardous ingredients.  Therefore we treat the strings
    `na` and `+secret-non-hazardous` as valid CAS numbers.

    >>> valid_CASno('na')
    True
    >>> valid_CASno('+secret-non-hazardous')
    True
    """
    if cas_string in [None, 'na', '+secret-non-hazardous']:
        return True
    # check format,  
    # \A matches the start of the string
    # \Z matches the end of the string
    if CAS_REGEXP.match(cas_string) == None:
        _LOG.debug("invalid CAS# format: '%s'".format(cas_string))
        return False
    # generate check digit
    casdigs = [int(d) for d in ''.join(cas_string.split('-'))]
    sumdigs = casdigs[:-1]
    sumdigs.reverse()
    s = sum([(i+1)*d for i,d in enumerate(sumdigs)])
    check = s % 10
    if check == casdigs[-1]:
        return True
    _LOG.debug("invalid CAS# check: '{}' (expected {})".format(
            cas_string, check))
    return False

def sanitize_path(string):
    for a,b in [('..', '-')]:
        string = string.replace(a, b)
    return _slugify(string)

def chemical_upload_to(instance, filename, storage=None):
    basename,extension = _os_path.splitext(filename)
    if extension not in ['.pdf', '.html', '.txt']:
        raise ValueError(filename)
    return 'msds/{}{}'.format(sanitize_path(instance.abbrev), extension)
